package com.example.cinemamvvm.network

import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface MyInterface {
    @POST("auth/login")
    fun loginRequest(@Body hashMap: HashMap<String, String>): Call<Login>
    @POST("auth/register")
    fun registerRequest(@Body hashMap: HashMap<String, String>): Call<Void>
    @GET("movies/cover")
    fun getCover(): Call<Cover>
    @GET("movies")
    fun getMovie(@Query("filter") filter: String): Call<Movies>
}