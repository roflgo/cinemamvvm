package com.example.cinemamvvm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView

class RecyclerActivity : AppCompatActivity(), RecyclerClick {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler)
        val orderlist= listOf<Order>(
            Order("Yaky","1"),
            Order("Yaqewrqweky","1"),
            Order("Yaky","1"),
            Order("Yrweqrweqrqweaky","1"),
            Order("Yaky","12"),
            Order("Yakweqrwqey","1"),
            Order("rewerqwrYaky","1"),
            Order("Yaky","1"),
        )
        val list = listOf<Courier>(
            Courier(1,"Olya",orderlist),
            Courier(2,"NeOlya",orderlist),
            Courier(3,"Olyag",orderlist),
            Courier(4,"Olyaa",orderlist),
        )
        val finishlist: MutableList<CourierForRecycler> = mutableListOf()
        list.map { courier->
            finishlist.add(courier.toCourierForRecycler())
            courier.orders.map { order->
                finishlist.add(
                    CourierForRecycler(
                        courierId = courier.courierId,
                        viewType = Type.Order,
                        orders = order
                    ))
            }
        }

        val recycler = findViewById<RecyclerView>(R.id.recycler)
        recycler.adapter = MyRecycler(finishlist,this)
    }

    override fun myClick(arg: String) {
        Toast.makeText(this, "123", Toast.LENGTH_SHORT).show()
    }


}