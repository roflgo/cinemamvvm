package com.example.cinemamvvm.presentation

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.example.cinemamvvm.common.Utils
import com.example.cinemamvvm.databinding.ActivitySignInBinding
import com.example.cinemamvvm.presentation.ViewModel.SignInViewModel
import java.util.regex.Pattern.compile

class SignInActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySignInBinding
    private lateinit var vm: SignInViewModel
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor
    private val EMAIL_PATTERN = Utils.EMAIL_PATTERN()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignInBinding.inflate(layoutInflater)
        sharedPreferences = getSharedPreferences("main",Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        vm = ViewModelProvider(this).get(SignInViewModel::class.java)
        setContentView(binding.root)

        binding.loginBtn.setOnClickListener {
            validateData()
        }

        binding.registerBtn.setOnClickListener{
            openActivityRegister()
        }

        vm.resultToken.observe(this) {
            editor.putString("token", it)
            editor.apply()
            openActivityMenu()
        }

        vm.errorTag.observe(this) {
            Utils.alertDialog(this, it)
        }

    }



    private fun validateData() {
        if (binding.loginForm.text.isNotEmpty() && binding.passwordForm.text.isNotEmpty()) {
            if (compile(EMAIL_PATTERN).matcher(binding.loginForm.text.toString()).matches()) {
                val hashMap: HashMap<String, String> = hashMapOf()
                hashMap.put("email", binding.loginForm.text.toString())
                hashMap.put("password", binding.passwordForm.text.toString())
                vm.loginRequest(hashMap)
            } else {
                Utils.alertDialog(this, "Введен некорректный email")
            }
        } else {
            Utils.alertDialog(this, "Все поля должны быть заполнены")
        }
    }

    private fun openActivityMenu() {
        val intent = Intent(this, MenuActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun openActivityRegister() {
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
    }
}