package com.example.cinemamvvm.presentation

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import com.example.cinemamvvm.R

class OnBoardingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_on_boarding)
        startTimer()
    }


    private fun startTimer() {
        val timerResult = object : CountDownTimer(2000, 100) {
            override fun onTick(p0: Long) {

            }

            override fun onFinish() {
                val intent = Intent(this@OnBoardingActivity, SignInActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
        timerResult.start()
    }
}