package com.example.cinemamvvm.presentation.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.cinemamvvm.*
import com.example.cinemamvvm.databinding.FragmentDashboardBinding

class DashboardFragment : Fragment(),RecyclerClick {

    private var _binding: FragmentDashboardBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val dashboardViewModel =
            ViewModelProvider(this).get(DashboardViewModel::class.java)

        _binding = FragmentDashboardBinding.inflate(inflater, container, false)
        val root: View = binding.root


        val orderlist= listOf<Order>(
            Order("Yaky","1"),
            Order("Yaqewrqweky","1"),
            Order("Yaky","1"),
            Order("Yrweqrweqrqweaky","1"),
            Order("Yaky","12"),
            Order("Yakweqrwqey","1"),
            Order("rewerqwrYaky","1"),
            Order("Yaky","1"),
        )
        val list = listOf<Courier>(
            Courier(1,"Olya",orderlist),
            Courier(2,"NeOlya",orderlist),
            Courier(3,"Olyag",orderlist),
            Courier(4,"Olyaa",orderlist),
        )
        val finishlist: MutableList<CourierForRecycler> = mutableListOf()
        list.map { courier->
            finishlist.add(courier.toCourierForRecycler())
            courier.orders.map { order->
                finishlist.add(
                    CourierForRecycler(
                        courierId = courier.courierId,
                        viewType = Type.Order,
                        orders = order
                    )
                )
            }
        }


        binding.recyclerView.adapter = MyRecycler(finishlist,this)
        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun myClick(arg: String) {
        findNavController().navigate(R.id.action_navigation_dashboard_to_navigation_home)
    }
}