package com.example.cinemamvvm.presentation.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.cinemamvvm.common.Utils
import com.example.cinemamvvm.network.Login
import com.example.cinemamvvm.network.MyRetrofit
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.HttpException
import retrofit2.Response
import java.lang.Exception
import java.util.regex.Pattern

class RegisterViewModel(
): ViewModel() {
    //val resultValide = MutableLiveData<String>()
    val errorTag = MutableLiveData<String>()
    val authSuccess = MutableLiveData<String>()

    fun authRequestValidate(name: String, surname: String, email: String, password: String, password_retry: String) {
        if (name.isNotEmpty()&&surname.isNotEmpty()&&email.isNotEmpty()&&password.isNotEmpty()&&password_retry.isNotEmpty()) {
            if (Pattern.compile(Utils.EMAIL_PATTERN()).matcher(email).matches()) {
                val hashMap: HashMap<String, String> = hashMapOf()
                hashMap.put("email", email)
                hashMap.put("password", password)
                hashMap.put("firstName", name)
                hashMap.put("lastName", surname)
                requestRegisterFun(hashMap)
            //Utils.alertDialog(this, "Введен неправильный email адрес")
            } else {
                errorTag.value = "Введен неправильный email адрес"
            }
        } else {
            errorTag.value = "Все поля должны быть заполнены"
        //Utils.alertDialog(this, "Все поля должны быть заполнены")
        }
    }

    fun requestRegisterFun(hashMap: HashMap<String, String>) {
        try {
            MyRetrofit.getRetrofit().registerRequest(hashMap)
                .enqueue(object : retrofit2.Callback<Void> {
                    override fun onResponse(call: Call<Void>, response: Response<Void>) {
                        if (response.isSuccessful) {
                            val hashMapLogin: HashMap<String, String> = hashMapOf()
                            hashMapLogin.put("email", hashMap.get("email").toString())
                            hashMapLogin.put("password", hashMap.get("password").toString())
                            loginRequest(hashMapLogin)
                        } else {
                            errorTag.value = "Ошибка ${response.body() ?: "null"}"
                        }
                    }

                    override fun onFailure(call: Call<Void>, t: Throwable) {
                        errorTag.value = "Произошла ошибка ${t.localizedMessage}"
                    }
                })
        } catch (e: Exception) {
            errorTag.value = "Произошла ошибка ${e.localizedMessage}"
        }
    }
//            if (call.isSuccessful) {
//                val hashMapLogin: HashMap<String, String> = hashMapOf()
//                hashMapLogin.put("email", hashMap.get("email").toString())
//                hashMapLogin.put("password", hashMap.get("password").toString())
//                loginRequest(hashMapLogin)
//            } else {
//                errorTag.value = "Ошибка ${call.body() ?: "null"}"
//            }
//        } catch (e: Exception){
//            errorTag.value = "Произошла ошибка ${e.localizedMessage}"
//        }


    fun loginRequest(hashMap: HashMap<String, String>) {
        MyRetrofit.getRetrofit().loginRequest(hashMap).enqueue(object : retrofit2.Callback<Login>{
            override fun onResponse(call: Call<Login>, response: Response<Login>) {
                if (response.isSuccessful) {
                    authSuccess.value = response.body()!!.token
                } else {
                    errorTag.value = "Ошибка ${response.body() ?: "null"}"
                }
            }

            override fun onFailure(call: Call<Login>, t: Throwable) {
                errorTag.value = "Произошла ошибка ${t.localizedMessage}"
            }
        })
    }
}